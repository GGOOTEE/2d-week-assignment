﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rB2D;

    public float runSpeed;
    public float jumpForce;
    public float threshold;
    private int count;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public SpriteRenderer spriteRenderer;
    public Animator animator;

    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        count = 0;
        SetCountText();
        winTextObject.SetActive(false);
    }

    void SetCountText()
    {
        countText.text = "Coins: " + count.ToString();
        if (count >= 5)
        {
            winTextObject.SetActive(true);
        }
    }

        void Update()
    {
        if( Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position,new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            
            jump();
        }
    }

    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput* runSpeed * Time.deltaTime, rB2D.velocity.y);

        if( rB2D.velocity.x < 0)
        {
            spriteRenderer.flipX = true;
        }
        else
        {
            spriteRenderer.flipX = false;
        }

        if (transform.position.y < threshold)
            transform.position = new Vector2(0, 1f);
    }

    void jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
        
            SetCountText();
        }
    }

   

}
